open! Base
open Common

module Artifact = struct
  type 'a custom = Custom of 'a

  type ('kind, 'how_made) t =
    | Custom :
        { id: 'a
        ; to_string: 'a -> string
        ; serialize: 'a -> 'b -> string
        ; deserialize_exn: 'a -> string -> 'b
        ; hash: 'a -> 'b -> string
        ; materialize: 'a -> 'b option }
        -> ('a custom, 'b) t
    | Pair : ('a, 'c) t * ('b, 'd) t -> ('a * 'b, 'c * 'd) t
    | List : ('a, 'c) t list -> ('a list, 'c list) t

  let rec to_string : type a b. (a, b) t -> string = function
    | Custom {id; to_string; _} -> to_string id
    | Pair (a, b) -> Fmt.str "(%s, %s)" (to_string a) (to_string b)
    | List l ->
        Fmt.str "[%s]" (List.map ~f:to_string l |> String.concat ~sep:"; ")

  let rec deserialize_exn : type a b. (a, b) t -> string -> b =
   fun a s ->
    match a with
    | Custom {id; deserialize_exn; _} -> deserialize_exn id s
    | Pair (a, b) ->
        let (astr, bstr) : string * string = Caml.Marshal.from_string s 0 in
        (deserialize_exn a astr, deserialize_exn b bstr)
    | List l ->
        let sl : string list = Caml.Marshal.from_string s 0 in
        List.map2_exn ~f:deserialize_exn l sl

  let rec serialize : type a b. (a, b) t -> b -> string =
   fun a b ->
    match (a, b) with
    | Custom {id; serialize; _}, b -> serialize id b
    | Pair (a, b), (sa, sb) ->
        Caml.Marshal.to_string (serialize a sa, serialize b sb) []
    | List l, sl -> Caml.Marshal.to_string (List.map2 ~f:serialize l sl) []

  let rec hash : type a b. (a, b) t -> b -> string =
   fun a b ->
    match (a, b) with
    | Custom {id; hash; _}, b -> hash id b
    | Pair (a, b), (sa, sb) ->
        Caml.Digest.(string (hash a sa ^ hash b sb) |> to_hex)
    | List l, sl ->
        let open Caml.Digest in
        string (List.map2_exn ~f:hash l sl |> String.concat ~sep:"") |> to_hex

  let rec materialize : type a b. (a, b) t -> b option =
   fun a ->
    match a with
    | Custom {id; materialize; _} -> materialize id
    | Pair (a, b) ->
        let open Option in
        materialize a >>= fun ma -> materialize b >>= fun mb -> return (ma, mb)
    | List l ->
        List.fold ~init:(Some []) l ~f:(fun prev m ->
            match prev with
            | None -> None
            | Some l -> (
              match materialize m with None -> None | Some v -> Some (v :: l) ))
        |> Option.map ~f:List.rev
end

module Action = struct
  type _ t = Ocaml : (unit -> 'a) -> 'a t

  let run : type a. a t -> a = function
    | Ocaml f ->
        let res = f () in
        res

  let to_string : type a. a t -> string = function Ocaml _ -> Fmt.str "OCAML"
end

module DAG = struct
  type _ t =
    | Return : ('a, 'b) Artifact.t * 'b -> ('a, 'b) Artifact.t t
    | Bind :
        ('a, 'b) Artifact.t t
        * (('a, 'b) Artifact.t -> 'b -> ('c, 'd) Artifact.t t)
        -> ('c, 'd) Artifact.t t
    | Join :
        ('a, 'b) Artifact.t t * ('c, 'd) Artifact.t t
        -> ('a * 'c, 'b * 'd) Artifact.t t
    | Join_list : ('a, 'b) Artifact.t t list -> ('a list, 'b list) Artifact.t t
    | Ensures : 'a Action.t * ('b, 'a) Artifact.t -> ('b, 'a) Artifact.t t
    | Annotation : string * 'a t -> 'a t
end

type ('a, 'b) build_status =
  {artifact: ('a, 'b) Artifact.t; value: 'b; done_something: bool}

module Hash_table = struct include Caml.Hashtbl end

module Database = struct
  type t =
    { mutable cache: (string, string) Hash_table.t
    ; mutable hashes: (string, string) Hash_table.t }

  let create () = {cache= Hash_table.create 2048; hashes= Hash_table.create 2048}
end

module State = struct
  type hold_artifact = Artifact : ('a, 'b) build_status -> hold_artifact

  type t =
    { depth: int
    ; stack: hold_artifact list
    ; database: Database.t
    ; log: string Queue.t
    ; previous_hashes: (string, string) Hash_table.t }

  let incr w = {w with depth= w.depth + 2}
  let push st art = {st with stack= Artifact art :: st.stack}

  let create previous_hashes =
    { depth= 0
    ; stack= []
    ; database= Database.create ()
    ; previous_hashes
    ; log= Queue.create () }

  let load f =
    let prev =
      try
        let open Caml in
        let i = open_in f in
        let v : (string, string) Hash_table.t = Marshal.from_channel i in
        close_in i ; v
      with _ -> Hash_table.create 10 in
    create prev

  let all_hashes s = s.database.hashes

  let save st f =
    let open Caml in
    let o = open_out f in
    Marshal.to_channel o (all_hashes st) [] ;
    close_out o ;
    ()

  let ps ?(with_stack = false) st fmt =
    let indent = String.make st.depth ' ' in
    Fmt.kstr
      (fun s ->
        Queue.enqueue st.log
          (Fmt.str "%s* %s\n%s%!" indent
             ( String.split ~on:'\n' s
             |> String.concat ~sep:(Fmt.str "\n%s" indent) )
             ( if with_stack then
               Fmt.str "%s\\[%s]\n%!" indent
                 ( List.map st.stack ~f:(function Artifact bs ->
                       Artifact.to_string bs.artifact)
                 |> String.concat ~sep:", " )
             else "" )))
      fmt

  let get_log st = st.log
  let lookup_cache st key = Hash_table.find_opt st.database.cache key

  type 'a looked_up = [`In_cache of 'a | `Not_in_cache of 'a | `Not_at_all]

  let lookup : type a b. t -> (a, b) Artifact.t -> b looked_up =
   fun st arti ->
    match
      ( Artifact.materialize arti
      , lookup_cache st (Artifact.to_string arti)
        |> Option.map ~f:(Artifact.deserialize_exn arti) )
    with
    | Some v, Some _ -> `In_cache v
    | None, Some v -> `In_cache v
    | Some v, None -> `Not_in_cache v
    | None, None -> `Not_at_all

  (* lookup_cache st (Artifact.to_string arti)
         * |> Option.map ~f:(Artifact.deserialize_exn arti) *)

  let get_hash st arti value =
    let key = Artifact.to_string arti in
    match Hash_table.find_opt st.database.hashes key with
    | Some s -> s
    | None ->
        let h = Artifact.hash arti value in
        Hash_table.replace st.database.hashes key h ;
        h

  let update_cache st arti value =
    let key = Artifact.to_string arti in
    let _ = get_hash st arti value in
    Hash_table.replace st.database.cache key (Artifact.serialize arti value)

  let dependencies_changed st =
    List.fold st.stack ~init:`No ~f:(fun prev ->
      function
      | Artifact bs -> (
          let new_hash = get_hash st bs.artifact bs.value in
          match
            ( prev
            , Hash_table.find_opt st.previous_hashes
                (Artifact.to_string bs.artifact) )
          with
          | `No, None -> `No_hash (Artifact.to_string bs.artifact)
          | `No, Some old_hash ->
              if String.(new_hash = old_hash) then `No
              else `Hash_changed (Artifact.to_string bs.artifact)
          | other, _ -> other ))
end

let ook artifact value done_something = Ok {artifact; value; done_something}

let rec build :
    type a b.
    _ -> (a, b) Artifact.t DAG.t -> ((a, b) build_status, Error.t) Result.t =
  let open Result in
  let open State in
  let open DAG in
  fun state -> function
    | Annotation (msg, x) ->
        ps state "<annotation %s|depth:%d|stack:%d|cache:%d>" msg state.depth
          (List.length state.stack)
          (Hash_table.length state.database.Database.cache) ;
        let r = build state x in
        ps state "</annotation %s|depth:%d|stack:%d>" msg state.depth
          (List.length state.stack) ;
        r
    | Return (a, v) -> (
        ps state "return %s" (Artifact.to_string a) ;
        try
          State.update_cache state a v ;
          ook a v false
        with e ->
          Error.exn e "Artifact returned but error: %s" (Artifact.to_string a) )
    | Bind (t, f) ->
        ps state "bind" ;
        build (incr state) t
        >>= fun bs ->
        (* ps state "built %s" (Artifact.to_string bs.v) ; *)
        build (push state bs |> incr) (f bs.artifact bs.value)
    | Join (a, b) ->
        ps state "Join" ;
        build (incr state) a
        >>= fun aa ->
        build (incr state) b
        >>= fun bb ->
        ook
          (Artifact.Pair (aa.artifact, bb.artifact))
          (aa.value, bb.value)
          (aa.done_something || bb.done_something)
    | Join_list al ->
        List.fold al ~init:(ook (Artifact.List []) [] false) ~f:(fun prev a ->
            prev
            >>= fun {artifact= Artifact.List l; value; done_something} ->
            build (incr state) a
            >>= fun aa ->
            return
              { artifact= Artifact.List (l @ [aa.artifact])
              ; value= value @ [aa.value]
              ; done_something= aa.done_something || done_something })
    | Ensures (m, a) ->
        (*
             For artifact a:
             - if not exists (in cache) -> build
             - if any dependency changed Vs DB -> build
             - otherwise return cached
             - then update cache
          *)
        let log = ref [] in
        let logf fmt = Fmt.kstr (fun s -> log := s :: !log) fmt in
        let actually_run () =
          logf "actually_run" ;
          try
            let b = Action.run m in
            State.update_cache state a b ;
            logf "success" ;
            ook a b true
          with e ->
            logf "error" ;
            Error.exn e "Ensuring %s" (Artifact.to_string a) in
        ( match State.lookup state a with
        | `In_cache b -> logf "in-cache" ; ook a b false
        | `Not_in_cache b -> (
            logf "not-in-cache" ;
            match State.dependencies_changed state with
            | `No_hash s -> logf "no-hash:%s" s ; actually_run ()
            | `Hash_changed s -> logf "hash-changed:%s" s ; actually_run ()
            | `No -> logf "no-dep-changed" ; ook a b false )
        | `Not_at_all -> logf "does-not-exist" ; actually_run () )
        >>= fun res ->
        ps ~with_stack:true state "to-ensure %s {%s}\n| %s"
          (Artifact.to_string a) (Action.to_string m)
          (List.rev_map !log ~f:(Fmt.str "%s") |> String.concat ~sep:" → ") ;
        Ok res
