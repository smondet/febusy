open! Base

let dbg fmt = Fmt.kstr (Fmt.epr "  BuildDBG: %s\n%!") fmt

module Error = struct
  type t = String of string | Exn of exn * string

  let string fmt = Fmt.kstr (fun s -> Error (String s)) fmt
  let exn e fmt = Fmt.kstr (fun s -> Error (Exn (e, s))) fmt
  let catch_exn f fmt = try Ok (f ()) with e -> exn e fmt

  let to_string = function
    | String s -> s
    | Exn (e, s) -> Fmt.str "Exception %a @ %s" Exn.pp e s

  let throw = function Ok o -> o | Error e -> failwith (to_string e)
end
