open Base
open Build

val return : ('a, 'b) Artifact.t -> 'b -> ('a, 'b) Artifact.t DAG.t

val ( >>= ) :
     ('a, 'b) Artifact.t DAG.t
  -> (('a, 'b) Artifact.t -> 'b -> ('c, 'd) Artifact.t DAG.t)
  -> ('c, 'd) Artifact.t DAG.t

val join_2 :
     ('a, 'b) Artifact.t DAG.t
  -> ('c, 'd) Artifact.t DAG.t
  -> ('a * 'c, 'b * 'd) Artifact.t DAG.t
(** [join_2 a b] is a node that depends on [a] and [b]. *)

val ( =<>= ) :
     ('a, 'b) Artifact.t DAG.t
  -> ('c, 'd) Artifact.t DAG.t
  -> ('a * 'c, 'b * 'd) Artifact.t DAG.t
(** [a =<>= b] is an alias for [join_2 a b]. *)

val join : ('a, 'b) Artifact.t DAG.t list -> ('a list, 'b list) Artifact.t DAG.t
(** [join l] is a node that depends on all the nodes in [l]. *)

val annotate : message:string -> 'a DAG.t -> 'a DAG.t

val ocaml : (unit -> 'a) -> 'a Action.t
(** Make an action from an OCaml function. *)

val ensures : o:('a, 'b) Artifact.t -> 'b Action.t -> ('a, 'b) Artifact.t DAG.t
(** Tie an artifact to an action that builds it, this a basic node in the DAG. *)

val ( <-- ) : ('a, 'b) Artifact.t -> (unit -> 'b) -> ('a, 'b) Artifact.t DAG.t
(** [a <-- f] is [ensures ~o (ocaml f)]. *)

val ( ** ) :
  ('a, 'b) Artifact.t -> ('c, 'd) Artifact.t -> ('a * 'c, 'b * 'd) Artifact.t
(** Build a “pair” compound artifact. *)

val list : ('a, 'b) Artifact.t list -> ('a list, 'b list) Artifact.t
(** Build a “list” compound artifact. *)

val artifact :
     ?serialize:('a -> 'b -> string)
  -> ?deserialize_exn:('a -> string -> 'b)
  -> 'a
  -> to_string:('a -> string)
  -> hash:('a -> 'b -> string)
  -> materialize:('a -> 'b option)
  -> ('a Artifact.custom, 'b) Artifact.t
(** Create a “root” build artifact. *)

(** The [File] module is a kind of artifact. *)
module File : sig
  type spec = File of {path: string}

  val create : string -> (spec Artifact.custom, [> `File of string]) Artifact.t

  val make :
       string
    -> (string -> unit)
    -> (spec Artifact.custom, [`File of string]) Artifact.t DAG.t

  val return :
    string -> (spec Artifact.custom, [`File of string]) Artifact.t DAG.t

  module List : sig
    val make :
         string list
      -> (string list -> unit)
      -> (spec Artifact.custom list, [> `File of string] list) Artifact.t DAG.t

    val return :
         string list
      -> (spec Artifact.custom list, [> `File of string] list) Artifact.t DAG.t
  end
end

(** The [String_value] module is a kind of artifact. *)
module String_value : sig
  type spec = String of {id: string}

  val create : string -> (spec Artifact.custom, string) Artifact.t
end

val file : string -> (File.spec Artifact.custom, [> `File of string]) Artifact.t
(** [file s] is an alias [File.create s] *)

val string : string -> (String_value.spec Artifact.custom, string) Artifact.t
(** [string s] is an alias [String_value.create s] *)

val return_value : string -> 'a -> (string Artifact.custom, 'a) Artifact.t DAG.t
(** [return_value id v] puts [v] in the dependency graph identified by
    [id] (in the cache and digest database). [v] needs to be
    serializable with the {!Marshal} module (and the digest is
    computed from the value too). *)

val phony :
  string -> (('a -> string) Build.Artifact.custom, unit) Build.Artifact.t
(** Artifact that is built “once-per-run.” *)

val return_fresh :
  'a -> (string Build.Artifact.custom, 'a) Build.Artifact.t Build.DAG.t
(** Return a value which is built “once-per-run” (has to be
    [Marshal]-serializable). *)

(** Useful functions to deal with the operating system. *)
module System : sig
  val home : unit -> string

  val cmdf :
       ?in_dir:string
    -> ?silent:bool
    -> ('a, Caml.Format.formatter, unit, unit) format4
    -> 'a

  val cmd_to_string_list : string -> string list
  val feed_cmd : string -> string -> unit
  val write_lines : string -> string list -> unit
  val read_lines : string -> string list
end

(** Run the build DAG in a mono-threaded Unix-ish environment. *)
module Make_unix : sig
  val run :
       ?state_file:string
    -> (unit -> ('a, 'b) Build.Artifact.t Build.DAG.t)
    -> (('a, 'b) Build.build_status, Common.Error.t) Result.t * string Queue.t
end
